<?php
/**
 * Created by PhpStorm.
 * User: Віталій
 * Date: 21.02.2017
 * Time: 21:10
 */

namespace app\models;
use yii\db\activeRecord;

class Category extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }
    public static function tableName(){
        return 'category';
    }

    public function getProducts(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

}