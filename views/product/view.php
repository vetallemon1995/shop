<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use ckarjun\owlcarousel\OwlCarouselWidget;

?><div class="col-sm-3 thumbnail">
    <ul class="nav nav-pills nav-stacked">
        <?= \app\components\MenuWidget::widget(['tpl'=>'menu']) ?>
    </ul>
</div>
<?php
$mainImg = $product->getImage();
$gallery = $product->getImages();
?>
<div class="col-sm-9 padding-right thumbnail">
    <h1><?=$product->name  ?></h1>
   <div class="col-sm-5">
       <?= Html::img($mainImg->getUrl(), ['alt' => $product->name,'class'=>'img-responsive'])?>

<?php
OwlCarouselWidget::begin([
    'container' => 'div',
    'containerOptions' => [
        'id' => 'slide',
        'class' => 'slider'
    ],
    'pluginOptions' => [
        'autoPlay' => 3000,
        'items' => 1,
        'itemsDesktop' => [84],
        'itemsDesktopSmall' => [84]
    ]
]);
?>


<div id="slide" class=" owl-theme slider">
           <?php $count = count($gallery);  foreach($gallery as $img): ?>

               <?= Html::img($img->getUrl('84'), ['alt' => ''])?>

           <?php endforeach;?>
</div>

<?php OwlCarouselWidget::end();?>

   </div>
<div class="col-sm-7">
    <div class="product-information"><!--/product-information-->

        <h2><?= $product->name?></h2>


									<span>
									<span><?= $product->price?> UAH</span>
									<label>Кількість:</label>
									<input type="text" value="1" id="qty" />
									<a href="<?= \yii\helpers\Url::to(['cart/add', 'id' => $product->id])?>" data-id="<?= $product->id?>" class="btn btn-default add-to-cart cart">
                                      <span class="glyphicon glyphicon-shopping-cart"></span>
                                      До корзини
                                    </a>
								</span>


        <p><b>Категорія:</b> <a href="<?= \yii\helpers\Url::to(['category/view', 'id' => $product->category->id]) ?>"><?= $product->category->name?></a></p>
    </div><!--/product-information-->






</div>
   <div class="col-sm-12">

           <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#description">Детальний опис</a></li>
               <li><a data-toggle="tab" href="#comments">Коментарі</a></li>

           </ul>

           <div class="tab-content">
               <div id="description" class="tab-pane fade in active">
                   <?= $product->content?>
               </div>
               <div id="comments" class="tab-pane fade">
                   comments
               </div>

           </div>


   </div>
</div>
