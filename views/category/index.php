<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

?><div class="col-sm-3 thumbnail">
    <ul class="nav nav-pills nav-stacked">
        <?= \app\components\MenuWidget::widget(['tpl'=>'menu']) ?>
    </ul>
    </div>

       <?php if( !empty($hits) ): ?>

        <div class="col-sm-9 padding-right thumbnail">
            <h2 class="title text-center">Популярні товари</h2>
            <?php  $i = 0; foreach($hits as $hit): ?>

        <?php $mainImg = $hit->getImage();?>
                <div class="col-sm-4 ">
                    <div class="product-image-wrapper thumbnail">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <?= Html::img($mainImg->getUrl(), ['alt' => $hit->name,'class'=>'img-responsive'])?>
                                <h2><?= $hit->price?>UAH</h2>
                                <p><a href="<?= \yii\helpers\Url::to(['product/view', 'id' => $hit  ->id]) ?>"><?= $hit->name?></a></p>
                                <a href="<?= \yii\helpers\Url::to(['cart/add', 'id' => $hit->id])?>" data-id="<?= $hit->id?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>До Корзини</a>
                            </div>
                        </div>
                    </div>
                </div>
        <?php $i++?>
        <?php if($i % 3 == 0): ?>
            <div class="clearfix"></div>
        <?php endif;?>
            <?php endforeach;?>

<?php endif; ?>