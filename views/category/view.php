<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

?><div class="col-sm-3 thumbnail">
<ul class="nav nav-pills nav-stacked">
    <?= \app\components\MenuWidget::widget(['tpl'=>'menu']) ?>
</ul>
</div>
<div class="col-sm-9 padding-right thumbnail">
<h1><?=$category->name  ?></h1>
        <?php if(!empty($products)): ?>
            <?php $i = 0; foreach($products as $product): ?>
                <?php $mainImg = $product->getImage();?>
                <div class="col-sm-4">
                    <div class="product-image-wrapper thumbnail">
                        <div class="single-products">
                            <div class="productinfo text-center">
                                <?= Html::img($mainImg->getUrl(), ['alt' => $hit->name,'class'=>'img-responsive'])?>
                                <h2><?= $product->price?>UAH</h2>
                                <p><a href="<?= \yii\helpers\Url::to(['product/view', 'id' => $product->id]) ?>"><?= $product->name?></a></p>
                                <a href="<?= \yii\helpers\Url::to(['cart/add', 'id' => $product->id])?>" data-id="<?= $product->id?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>До корзини</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++?>
                <?php if($i % 3 == 0): ?>
                    <div class="clearfix"></div>
                <?php endif;?>
            <?php endforeach;?>
            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>
        <?php else :?>
            <h2>Товарів не має</h2>
        <?php endif;?>
        <div class="clearfix"></div>

